// ==UserScript==
// @name chess.com Profile Title Status
// @icon https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Simple_bronze_crown.svg/128px-Simple_bronze_crown.svg.png
// @namespace    http://tampermonkey.net/
// @description    `fake status for chess.com`
// @version        1.0.0
// @grant    none
// @match           *://www.chess.com/member/tio_ali
// @run-at          document-end
// ==/UserScript==
var titlestatus = document.querySelector('div.profile-info-row');
titlestatus.insertAdjacentHTML('afterend', '<div class="status-component status-titled"><div class="icon-font-component status-membership-icon"><span class="icon-font-chess chess-crown"></span></div><div class="status-label-group"><span class="status-label">Jogador Titulado</span></div><a href="https://www.chess.com/members/titled-players" class="status-data status-stream-url" rel="nofollow">FIDE ONLINE ARENA: ACM</a></div><div class="status-component status-diamond"><div class="icon-font-component status-membership-icon"><span class="icon-font-chess membership-diamond"></span></div><div class="status-label-group"><span class="status-label">Membro Diamante</span></div><a href="https://www.chess.com/membership?c=icon" class="status-data status-stream-url" rel="nofollow">Desde 31 de dez. de 2020</a></div>');