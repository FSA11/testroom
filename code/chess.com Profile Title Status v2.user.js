// ==UserScript==
// @name chess.com Profile Title Status v2
// @icon https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Simple_bronze_crown.svg/128px-Simple_bronze_crown.svg.png
// @namespace    ChessSkins
// @description    `fake status for chess.com`
// @author          agiota_do_artenio
// @homepage        https://www.chess.com/blog/Agiota_do_Artenio
// @version        2.0.0
// @grant    none
// @match           *://www.chess.com/member/agiota_do_artenio
// @run-at          document-end
// ==/UserScript==
var titlestatus = document.querySelector('div.v5-section.v5-overflow-hidden.profile-about');
titlestatus.insertAdjacentHTML('afterend', '<div class="status-component status-titled"><div class="icon-font-component status-membership-icon"><span class="icon-font-chess chess-crown"></span></div><div class="status-label-group"><span class="status-label">Jogador Titulado</span></div><a href="https://www.chess.com/members/titled-players" class="status-data status-stream-url" rel="nofollow">Skin Master</a></div><div class="status-component status-diamond"><div class="icon-font-component status-membership-icon"><span class="icon-font-chess membership-diamond"></span></div><div class="status-label-group"><span class="status-label">Membro da Máfia</span></div><a href="https://www.chess.com/membership?c=icon" class="status-data status-stream-url" rel="nofollow">Desde 19 de mar. de 2021</a></div><div class="status-component  status-top-blogger"><span class="icon-font-chess status-membership-icon paper-pencil "></span><div class="status-label-group"><span class="status-label">Melhor Blogueiro(a)</span></div><div class="status-data-wrapper"><a href="https://www.chess.com/blog/Agiota_do_Artenio" class="status-data status-stream-url" rel="nofollow"> www.chess.com/blog/Agiota_do_Artenio</a></div> </div>');