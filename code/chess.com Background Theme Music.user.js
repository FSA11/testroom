// ==UserScript==
// @name chess.com Background Theme Music
// @icon https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Simple_bronze_crown.svg/128px-Simple_bronze_crown.svg.png
// @namespace    ChessSkins
// @description    `Play some background theme music in loop`
// @author          agiota_do_artenio
// @homepage        https://www.chess.com/blog/Agiota_do_Artenio
// @version        1.0.0
// @grant    none
// @match           *://www.chess.com/member/agiota_do_artenio
// @run-at          document-end
// ==/UserScript==
var themesound = document.querySelector('body');
themesound.insertAdjacentHTML('afterend', '<embed src="https://archive.org/download/TheGodfatherOriginalThemeSong/The%20Godfather%20Original%20Theme%20Song.ogg" loop="true" autostart="true" width="2" height="0">');