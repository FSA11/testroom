// ==UserScript==
// @name chess.com Video Playback
// @icon https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Simple_bronze_crown.svg/128px-Simple_bronze_crown.svg.png
// @namespace    ChessSkins
// @description    `Play some video on live game page`
// @author          agiota_do_artenio
// @homepage        https://www.chess.com/blog/Agiota_do_Artenio
// @version        1.0.2
// @grant    none
// @match           *://www.chess.com/game/live*
// @match           *://www.chess.com/live*
// @run-at        document-end
// ==/UserScript==
var themesound = document.querySelector('.sidebar-component .sidebar-tabsetTop');
themesound.insertAdjacentHTML('beforebegin', '<iframe width="500" height="400" src="https://www.youtube.com/embed/469oNbcsF6w?autoplay=1&mute=1" title="YouTube video player" frameborder="0" allow="accelerometer; allow="autoplay"; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="display: block"></iframe>');
var forceStyle = document.querySelector('#cdm-zone-end');
forceStyle.insertAdjacentHTML('afterend', '<style>iframe{display:block!important;}</style>');
