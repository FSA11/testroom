// ==UserScript==
// @name chess.com Twitch On Live Page [GMKrikor]
// @icon https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Simple_bronze_crown.svg/128px-Simple_bronze_crown.svg.png
// @namespace    ChessSkins
// @description    `Play some video on live game page`
// @author          agiota_do_artenio
// @homepage        https://www.chess.com/blog/Agiota_do_Artenio
// @version        1.0.0
// @grant    none
// @match           *://www.chess.com/live*
// @run-at        document-end
// ==/UserScript==
var themesound = document.querySelector('.sidebar-component .sidebar-tabsetTop');
themesound.insertAdjacentHTML('beforebegin', '<iframe allowfullscreen="allowfullscreen" allow="autoplay; fullscreen" frameborder="0" height="400" scrolling="no" width="500" class="tv-player-iframe" data-chess-src="//player.twitch.tv/?channel=gmkrikor&amp;autoplay=true&amp;muted=false&amp;parent=www.chess.com" data-chess-srcset="undefined" src="//player.twitch.tv/?channel=gmkrikor&amp;autoplay=true&amp;muted=false&amp;parent=www.chess.com" srcset="//player.twitch.tv/?channel=gmkrikor&amp;autoplay=true&amp;muted=false&amp;parent=www.chess@2x.com 2x" data-visible="true"></iframe>');
var forceStyle = document.querySelector('#cdm-zone-end');
forceStyle.insertAdjacentHTML('afterend', '<style>iframe{display:block!important;}</style>');
