// ==UserScript==
// @name chess.com Sid Ali Profile
// @icon https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Simple_bronze_crown.svg/128px-Simple_bronze_crown.svg.png
// @namespace    ChessSkins
// @description    `Sid Ali Profile with music`
// @author          agiota_do_artenio
// @homepage        https://www.chess.com/blog/Agiota_do_Artenio
// @version        1.0.2
// @grant    none
// @match           *://www.chess.com/game/live*
// @match           *://www.chess.com/live*
// @match           *://www.chess.com/member/tio_ali
// @run-at          document-end
// ==/UserScript==
var themesound = document.querySelector('body');
themesound.insertAdjacentHTML('afterend', '<audio id="musicplayer" autoplay><source src="https://gitlab.com/FSA11/testroom/-/raw/main/personalities/o_clone/music/04.ogg"></audio>');
